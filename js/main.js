$(document).ready(function() {

        var owl = $("#slider");

        // $("a[href='#top']").on('click touch', function() {
        //     $("html, body").animate({ scrollTop: 0 }, "slow");
        //     return false;
        // });

        $("#slider").owlCarousel({
                items: 3,
                pagination: false,
                nav: true,
                dots: false,
                loop: true,
                navText: ["<img src='images/arrow2.png'>", "<img src='images/arrow.png'>"],
                responsiveClass: true,
                responsive: {
                        0: {
                                items: 1,
                        },
                        481: {
                                items: 2,

                        },
                        767: {
                                items: 3,
                        }
                }
        });

        $(".next").click(function() {
                owl.trigger('owl.next');
        })
        $(".prev").click(function() {
                owl.trigger('owl.prev');
        })

        $('.link').on('click touch', function() {
                $(this).closest('.car-block').find('a')[0].click();
        });
});
